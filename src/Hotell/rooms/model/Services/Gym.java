package Hotell.rooms.model.Services;

import java.util.*;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.IntegerProperty;


public class Gym implements ServiceInterface
{
    private  List<IntegerProperty> previousPrices;
    private Map<String, IntegerProperty> exercise_machines;
    private IntegerProperty discount = new SimpleIntegerProperty(0);

    public Gym()
    {
        exercise_machines.put("Exercise Bike", new SimpleIntegerProperty(5));
        exercise_machines.put("Running track", new SimpleIntegerProperty(7));
        exercise_machines.put("power trainer", new SimpleIntegerProperty(6));
        exercise_machines.put("Trainer", new SimpleIntegerProperty(20));
        for(String a: exercise_machines.keySet())
        {
            previousPrices.add(exercise_machines.get(a));
        }

    }

    @Override
    public int ReturnDiskount() {
        return 0;
    }

    public void DoDiscounts(int percent)
    {
        for(String a: exercise_machines.keySet())
        {
            exercise_machines.replace(a, new SimpleIntegerProperty(exercise_machines.get(a).getValue().intValue()-exercise_machines.get(a).getValue().intValue() * percent / 100));
        }
        discount = new SimpleIntegerProperty(percent);
    }
    public void ReturnPrices()
    {
        int counter = 0;
        for(String a: exercise_machines.keySet())
        {
            exercise_machines.replace(a, previousPrices.get(counter));
            counter++;
        }
        discount = new SimpleIntegerProperty(0);
    }
    public double Count(ListOfServices L)
    {
        int bill = 0;
        Map<String, IntegerProperty> l = L.getMyList();
        for(String a: l.keySet())
        {
            if (exercise_machines.containsKey(a) == true)
            {
                bill = bill + l.get(a).getValue().intValue();
                L.Remove(a, l.get(a));
            }
        }
        return bill;
    }
}
