package Hotell.rooms.model.Services;

import java.util.*;

import javafx.beans.property.*;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Cafe implements ServiceInterface
{
    private List<SimpleIntegerProperty> previousPrices = new LinkedList<SimpleIntegerProperty>();
    private Map<String, SimpleIntegerProperty> menu = new HashMap<String, SimpleIntegerProperty>();
    private IntegerProperty discount = new SimpleIntegerProperty(0);
    private ObservableList<CafeMenu> m =  FXCollections.observableArrayList();

    public Cafe()
    {
        menu.put("soup", new SimpleIntegerProperty(5));
        menu.put("ice cream", new SimpleIntegerProperty(7));
        menu.put("cofee", new SimpleIntegerProperty(2));
        menu.put("tea", new SimpleIntegerProperty(2));
        for(String a: menu.keySet())
        {
            previousPrices.add(menu.get(a));
        }
        for(String a: menu.keySet())
        {
            m.add(new CafeMenu(menu.get(a).getValue().intValue(),a));
        }
    }
    public ObservableList<CafeMenu> GetList()
    {
        return m;
    }
    public int ReturnDiskount()
    {
        return discount.getValue();
    }
    public void Add (CafeMenu m)
    {
        menu.put(m.getName(),new SimpleIntegerProperty(m.getPrice()));
    }
    public class CafeMenu
    {
        private SimpleIntegerProperty price;
        private StringProperty name;

        public CafeMenu(int price, String name)
        {
            this.price = new SimpleIntegerProperty(price);
            this.name = new SimpleStringProperty(name);
        }

        public String getName()
        {
            return name.get();
        }
        public Integer getPrice()
        {
            return price.get();
        }

        public void setPrice(int price) {
            this.price.set(price);
        }

        public void setName(String name) {
            this.name.set(name);
        }
    }
    public void DoDiscounts(int percent)
    {
        // m.removeAll();
        for(String a: menu.keySet())
        {
            m.remove(0);
            menu.replace(a, new SimpleIntegerProperty(menu.get(a).getValue().intValue()-menu.get(a).getValue().intValue() * (percent - 5) / 100));
            this.m.add(new CafeMenu(menu.get(a).getValue().intValue(),a));
        }
        discount = new SimpleIntegerProperty(percent);

    }
    public void ReturnPrices()
    {
        int counter = 0;
        m.removeAll();
        for(String a: menu.keySet())
        {
            m.remove(0);
            menu.replace(a, previousPrices.get(counter));
            counter++;
            m.add(new CafeMenu(menu.get(a).getValue().intValue(),a));
        }
        discount = new SimpleIntegerProperty(0);
    }
    public double Count(ListOfServices L)
    {
        int bill = 0;
        HashMap<String, IntegerProperty> l = L.getMyList();
        List<String> copy = new LinkedList<String>();
        for(String a: l.keySet())
        {
            if (menu.containsKey(a) == true)
            {
                bill = bill + l.get(a).getValue().intValue();
                copy.add(a);
            }
        }
        int len = l.keySet().size();
        for(String b:copy)
        {
            L.Remove(b);
        }
        return bill + bill * 0.15;
    }
}
