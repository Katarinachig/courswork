package Hotell.rooms.model.Services;

import javafx.beans.property.IntegerProperty;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ListOfServices
{
    private HashMap<String, IntegerProperty> myList;

    public void setMyList(HashMap<String, IntegerProperty> myList)
    {
        this.myList = myList;
    }

    public HashMap<String, IntegerProperty> getMyList()
    {
        return myList;
    }
    public ListOfServices()
    {
        myList = new HashMap<String, IntegerProperty>();
    }
    public void Add(String s, IntegerProperty a)
    {
        this.myList.put(s,a);
    }
    public void Remove(String s, IntegerProperty a)
    {
        this.myList.remove(s,a);
    }
    public void Remove(String s)
    {
        this.myList.remove(s);
    }
}