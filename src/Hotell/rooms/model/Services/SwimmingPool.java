package Hotell.rooms.model.Services;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import java.util.List;
import java.util.Map;

public class SwimmingPool implements ServiceInterface
{
    private IntegerProperty discount = new SimpleIntegerProperty(0);
    private IntegerProperty price_for_minute = new SimpleIntegerProperty(3);
    private IntegerProperty previous_price;


    @Override
    public int ReturnDiskount() {
        return 0;
    }

    public SwimmingPool()
    {
        previous_price.setValue(price_for_minute.getValue().intValue());
    }
    public void DoDiscounts(int percent)
    {
        price_for_minute.setValue(price_for_minute.getValue().intValue() - price_for_minute.getValue().intValue()* percent/100);
        discount = new SimpleIntegerProperty(percent);
    }
    public void ReturnPrices()
    {
        price_for_minute.setValue(previous_price.getValue().intValue());
        discount = new SimpleIntegerProperty(0);
    }
    public double Count(ListOfServices L)
    {
        int bill = 0;
        Map<String, IntegerProperty> l = L.getMyList();
        if (l.containsKey("swimm"))
        {
            bill = bill + l.get("swimm").getValue().intValue();
            L.Remove("swimm", l.get("swimm"));
        }
        return bill;
    }
}