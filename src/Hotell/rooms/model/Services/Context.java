package Hotell.rooms.model.Services;

import javafx.beans.property.IntegerProperty;

import java.util.List;
import java.util.Map;

public class Context
{
    private ServiceInterface service;

    public  void SetStrategy(ServiceInterface service)
    {
        this.service = service;
    }
    public void Count (ListOfServices L)
    {
        service.Count(L);
    }
    public void ReturnPrices ()
    {
        service.ReturnPrices();
    }
    public void DoDiscounts (int percent)
    {
        service.DoDiscounts(percent);
    }
}
