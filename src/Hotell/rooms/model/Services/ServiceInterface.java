package Hotell.rooms.model.Services;

import javafx.beans.property.IntegerProperty;
import Hotell.rooms.model.Services.ListOfServices;

import java.util.List;
import java.util.Map;

public interface ServiceInterface
{
    void DoDiscounts(int percent);
    void ReturnPrices();
    double Count(ListOfServices l);
    int ReturnDiskount();
}
