package Hotell.rooms.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;

public class Room {
    private IntegerProperty id;
    private IntegerProperty price;
    private StringProperty empty;
    private IntegerProperty numrer_of_rooms;
    private StringProperty name;
    private static int roomId = 0;

    public Room()
    {
        this(100, "yes", 2, null);
    }

    public Room(int price, String empty, int numbers_of_rooms, String name)
    {
        this.price = new SimpleIntegerProperty(price);
        this.empty = new SimpleStringProperty(empty);
        this.numrer_of_rooms = new SimpleIntegerProperty(numbers_of_rooms);
        this.name = new SimpleStringProperty(name);
        this.id = new SimpleIntegerProperty(roomId);
        roomId++;
    }

    public String getName()
    {
        return this.name.get();
    }

    public void setName(String name)
    {
        this.name.set(name);
    }

    public void setNumber_of_rooms(int numrer_of_rooms) {
        this.numrer_of_rooms.set(numrer_of_rooms);
    }
    public Integer getNumber_of_rooms()
    {
        return numrer_of_rooms.get();
    }

    public Integer getPrice() {
        return price.get();
    }

    public void setPrice(int price) {
        this.price.set(price);
    }

    public Integer getId() {
        return id.get();
    }

    public void setId()
    {
        this.id.set(roomId);
        roomId++;
    }

    public void setEmpty(String empty)
    {
        this.empty.set(empty);
    }
    public String getEmpty()
    {
        return this.empty.get();
    }
    public int PriceForRoom(int days)
    {
        int totalPrice = 0;
        totalPrice += days * this.getPrice();
        return totalPrice;
    }

}

