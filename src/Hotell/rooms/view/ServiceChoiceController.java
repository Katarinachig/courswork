package Hotell.rooms.view;

import Hotell.rooms.Main;
import Hotell.rooms.model.Room;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class ServiceChoiceController
{
    @FXML
    private Button Gym;
    @FXML
    private Button Cafe;
    @FXML
    private Button SwimmingPool;
    @FXML
    private Main main;
    public void setMain(Main main)
    {
        this.main = main;
    }
    private void initialize()
    {
    }
    /*
    @FXML
    private void handleGym()
    {
        main.showGym();
    }*/
    @FXML
    private void handleCafe()
    {
        main.showCafe();
    }
   /* @FXML
    private void handleSwimmingPool()
    {
        main.showSwimmingPool();
    }
    */

}