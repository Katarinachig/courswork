package Hotell.rooms.view;

import Hotell.rooms.model.Services.Cafe;
import Hotell.rooms.model.Services.ServiceInterface;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import Hotell.rooms.view.CafeOverviewController;

public class DiskountController
{
    private Stage dialogStage;
    private boolean okClicked = false;
    @FXML
    private TextField percentField;
    @FXML
    private void initialize()
    {
    }
    CafeOverviewController cafe;
    ServiceInterface si;

    public void setSi(ServiceInterface si) {
        this.si = si;
    }

    public void setDialogStage(Stage dialogStage)
    {
        this.dialogStage = dialogStage;
    }

    public void setCafe(CafeOverviewController cafe) {
        this.cafe = cafe;
    }

    public boolean isOkClicked() {
        return okClicked;
    }
    @FXML
    private void handleOk()
    {
        if (isInputValid())
        {
            int p = Integer.parseInt(percentField.getText());
            si.DoDiscounts(p);
            cafe.ChangeTable();
            okClicked = true;
            dialogStage.close();
        }
    }


    @FXML
    private void handleCancel()
    {
        dialogStage.close();
    }

    private boolean isInputValid()
    {
        String errorMessage = "";
        if (si.ReturnDiskount() != 0)
        {
            errorMessage += "Discount has already done\n";
        }
        if (percentField.getText().length() == 0 || percentField.getText() == null) {
            errorMessage += "No valid percent!\n";
        } else {
            try {
                Integer.parseInt(percentField.getText());
                if((Integer.parseInt(percentField.getText())< 0) ||(Integer.parseInt(percentField.getText())>90))
                {
                    errorMessage+="Too big or too small percents";
                }
            } catch (NumberFormatException e) {
                errorMessage += "No valid percent (must be an integer)!\n";
            }
        }
        if (errorMessage.length() == 0)
        {
            return true;
        } else
        {
            // Показываем сообщение об ошибке.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Field");
            alert.setHeaderText("Please correct invalid percent");
            alert.setContentText(errorMessage);
            alert.showAndWait();
            return false;
        }
    }
}
