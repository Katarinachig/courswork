package Hotell.rooms.view;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import Hotell.rooms.Main;

public class FirstPageController
{
    @FXML
    private Button priceField;
    @FXML
    private Button emptyField;
    @FXML
    private Main main;
    public void setMain(Main main)
    {
        this.main = main;
    }
    private void initialize()
    {
    }
    @FXML
    private void handleRoom()
    {
        main.showRooms();
    }
    @FXML
    private void handleServices()
    {
        main.showServices();
    }
}