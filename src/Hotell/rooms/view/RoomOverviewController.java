package Hotell.rooms.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.collections.ObservableList;
import Hotell.rooms.Main;
import Hotell.rooms.model.Room;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class RoomOverviewController
{
    @FXML
    private TableView<Room> roomTable;
    @FXML
    private TableColumn<Room, String> roomNameColumn;
    @FXML
    private TableColumn<Room, Integer > idColumn;
    @FXML
    private Label priceLabel;
    @FXML
    private Label emptyLabel;
    @FXML
    private Label numberLabel;
    private Stage dialogStage;
    private Main main;
    public RoomOverviewController()
    {
    }
    @FXML
    private void initialize()
    {
        roomNameColumn.setCellValueFactory(new PropertyValueFactory<Room, String>("name"));
        idColumn.setCellValueFactory(new PropertyValueFactory<Room, Integer>("id"));
        showRoomDetails(null);
        // Слушаем изменения выбора
        roomTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showRoomDetails(newValue));

    }
    public void setMain(Main main)
    {
        this.main = main;
        ObservableList<Room> roomData1 = main.getRoomData();
        roomTable.setItems(roomData1);
    }
    public void setDialogStage(Stage dialogStage)
    {
        this.dialogStage = dialogStage;
    }
    private void showRoomDetails(Room room)
    {
        if (room != null) {
            // Заполняем метки информацией из объекта person.
            priceLabel.setText(Integer.toString(room.getPrice()));
            emptyLabel.setText(room.getEmpty());
            numberLabel.setText(Integer.toString(room.getNumber_of_rooms()));
        }
        else {
            // Если Person = null, то убираем весь текст.
            priceLabel.setText("");
            emptyLabel.setText("");
            numberLabel.setText("");
        }
    }
    @FXML
    private void handleDeleteRoom()
    {
        int selectedIndex = roomTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex > 0)
            roomTable.getItems().remove(selectedIndex);
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(main.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Person Selected");
            alert.setContentText("Please select a person in the table.");
            alert.showAndWait();
        }
    }

    @FXML
    private void handleNewRoom() {
        Room tempRoom = new Room();
        boolean okClicked = main.showRoomEditDialog(tempRoom);
        if (okClicked)
        {
            main.getRoomData().add(tempRoom);
        }
        // dialogStage.close();
    }

    @FXML
    private void handleEditRoom()
    {
        Room selectedRoom = roomTable.getSelectionModel().getSelectedItem();
        if (selectedRoom != null && selectedRoom.getEmpty() == "yes")
        {
            boolean okClicked = main.showRoomEditDialog(selectedRoom);
            if (okClicked)
            {
                showRoomDetails(selectedRoom);
            }
            // dialogStage.close();
        } else {
            // Ничего не выбрано.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(main.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No room selected or room is not empty");
            alert.setContentText("Please select an empty room");
            alert.showAndWait();
        }
    }

}