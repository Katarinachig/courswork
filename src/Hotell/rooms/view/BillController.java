package Hotell.rooms.view;

import Hotell.rooms.model.Services.ListOfServices;
import Hotell.rooms.model.Services.ServiceInterface;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;


public class BillController
{
    @FXML
    private TextArea bill;
    private ListOfServices ls;
    private Stage dialogStage;
    private double rez;

    public void setRez(double rez) {
        this.rez = rez;
    }

    public void setDialogStage(Stage dialogStage)
    {
        this.dialogStage = dialogStage;
    }

    public void setLs(ListOfServices ls)
    {
        this.ls = ls;
    }
    @FXML
    private void initialize()
    {

    }
    private final static String newline = "\n";
    @FXML
    private void handleOk()
    {
        dialogStage.close();
    }
    public void Show(ListOfServices ls, double rez)
    {
        for(String a: ls.getMyList().keySet())
        {
            bill.appendText(a + "..............."+Integer.toString(ls.getMyList().get(a).getValue())+newline);
        }
        bill.appendText("price for all...........");
        bill.appendText(Double.toString(rez));
    }
}
