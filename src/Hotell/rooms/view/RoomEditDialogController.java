package Hotell.rooms.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import Hotell.rooms.model.Room;

public class RoomEditDialogController
{
    @FXML
    private TextField priceField;
    @FXML
    private TextField emptyField;
    @FXML
    private TextField numberField;
    @FXML
    private TextField nameField;

    private Stage dialogStage;
    private Room room;
    private boolean okClicked = false;

    @FXML
    private void initialize()
    {
    }
    public void setDialogStage(Stage dialogStage)
    {
        this.dialogStage = dialogStage;
    }

    public void setRoom(Room room) {
        this.room= room;

        priceField.setText(Integer.toString(room.getPrice()));
        emptyField.setText(room.getEmpty());
        numberField.setText(Integer.toString(room.getNumber_of_rooms()));
        nameField.setText(room.getName());
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleOk() {
        if (isInputValid())
        {
            room.setName(nameField.getText());
            room.setPrice(Integer.parseInt(priceField.getText()));
            room.setName(nameField.getText());
            room.setNumber_of_rooms(Integer.parseInt(numberField.getText()));
            okClicked = true;
            dialogStage.close();
        }
    }


    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (nameField.getText() == null || nameField.getText().length() == 0) {
            errorMessage += "No valid room name!\n";
        }
        if (priceField.getText() == null || priceField.getText().length() == 0) {
            errorMessage += "No valid price!\n";
        } else {
            try {
                Integer.parseInt(priceField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "No valid price (must be an integer)!\n";
            }
        }
        if (emptyField.getText() == null || emptyField.getText().length() == 0) {
            errorMessage += "No valid value for empty!\n";
        }
        if (numberField.getText() == null || numberField.getText().length() == 0) {
            errorMessage += "No valid rooms quantity!\n";
        } else {
            try {
                Integer.parseInt(numberField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "No valid price (must be an integer)!\n";
            }
        }
        if (errorMessage.length() == 0)
        {
            return true;
        } else
        {
            // Показываем сообщение об ошибке.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);
            alert.showAndWait();
            return false;
        }
    }

}