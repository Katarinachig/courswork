package Hotell.rooms.view;

import Hotell.rooms.Main;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import Hotell.rooms.model.Services.Cafe;
import Hotell.rooms.model.Services.ServiceInterface;
import Hotell.rooms.model.Services.ListOfServices;
import javafx.stage.WindowEvent;
import javafx.event.EventHandler;

import java.util.Collections;
import java.util.HashMap;

public class CafeOverviewController
{
    @FXML
    private TableView<Cafe.CafeMenu> menuTable;
    @FXML
    private TableColumn<Cafe.CafeMenu, String> dishNameColumn;
    @FXML
    private TableColumn<Cafe.CafeMenu, Integer > priceColumn;
    @FXML
    private Button selectButton;
    @FXML
    private Button newButton;
    @FXML
    private Button doDiscountButton;
    @FXML
    private Button PayButton;
    @FXML
    private Button returnPricesButton;
    private Stage dialogStage;
    private Cafe c;
    private Main main;
    ListOfServices ls;
    public  void setMain(Main m)
    {
        this.main = m;
    }

    public void setDialogStage(Stage dialogStage)
    {
        this.dialogStage = dialogStage;
    }

    public void setCafe(Cafe c)
    {
        this.c = c;
        menuTable.setItems(c.GetList());
    }
    @FXML
    private void initialize()
    {

        ls = new ListOfServices();
        dishNameColumn.setCellValueFactory(new PropertyValueFactory<Cafe.CafeMenu, String>("name"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<Cafe.CafeMenu, Integer>("price"));
        // Слушаем изменения выбора
        //  menuTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showRoomDetails(newValue));

    }
    @FXML
    private void doDiscountHandle()
    {
        main.showDiscount(c, this);
    }
    @FXML
    private void ReturnPriceHandle()
    {
        c.ReturnPrices();
        ChangeTable();
    }
    @FXML
    public void ChangeTable()
    {
        // menuTable.getItems().clear();
        menuTable.setItems(c.GetList());
    }
    public void setAction()
    {
        dialogStage.setOnCloseRequest(closeEventHandler);
    }
    @FXML
    public void SelectHandle()
    {
        Cafe.CafeMenu cm = menuTable.getSelectionModel().getSelectedItem();
        ls.Add(cm.getName(), new SimpleIntegerProperty(cm.getPrice()));
    }

    @FXML
    public void PayHandle()
    {
        ListOfServices ls1 = new ListOfServices();
        ls1.setMyList((HashMap)ls.getMyList().clone());
        double rez = c.Count(ls);
        main.showBill(ls1, rez);
        dialogStage.close();
    }

    private EventHandler<WindowEvent> closeEventHandler = new EventHandler<WindowEvent>() {
        @Override
        public void handle(WindowEvent event)
        {
            PayHandle();
        }
    };
}