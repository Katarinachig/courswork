package Hotell.rooms;

import java.io.IOException;

import Hotell.rooms.model.Room;
import Hotell.rooms.model.Services.Cafe;
import Hotell.rooms.model.Services.ListOfServices;
import Hotell.rooms.model.Services.ServiceInterface;
import Hotell.rooms.view.FirstPageController;
import Hotell.rooms.view.RoomEditDialogController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import Hotell.rooms.view.RoomOverviewController;
import javafx.stage.Modality;
import Hotell.rooms.view.*;

public class Main extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private ObservableList<Room> roomData =  FXCollections.observableArrayList();
    private Cafe c;

    public Main()
    {
        roomData.add(new Room(100, "yes", 2, "red_room"));
        roomData.add(new Room(200, "no", 1, "gold_room"));
        roomData.add(new Room(150, "yes", 3, "black_room"));
        c = new Cafe();
    }

    public ObservableList<Room> getRoomData()
    {
        return roomData;
    }
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("HotelApp");
        initRootLayout();
        showFirstPageOverview();
    }

    public void initRootLayout()
    {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showFirstPageOverview()
    {
        try {
            FXMLLoader loader1 = new FXMLLoader();
            loader1.setLocation(Main.class.getResource("view/FirstPage.fxml"));
            GridPane firstPageOverview = (GridPane) loader1.load();
            rootLayout.setCenter(firstPageOverview);
            FirstPageController controller = loader1.getController();
            controller.setMain(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Stage getPrimaryStage()
    {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public boolean showRoomEditDialog(Room room) {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/RoomEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Room");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            RoomEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setRoom(room);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();

            return controller.isOkClicked();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
    }
    public void showRooms()
    {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/RoomOverview.fxml"));
            AnchorPane roomOverview = (AnchorPane) loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Show Room");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(roomOverview);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            RoomOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMain(this);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showServices()
    {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/ServiceChoice.fxml"));
            GridPane serviceChoice = (GridPane) loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Services choice");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(serviceChoice);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            ServiceChoiceController controller = loader.getController();
            //controller.setDialogStage(dialogStage);
            controller.setMain(this);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showCafe()
    {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/CafeOverview.fxml"));
            GridPane serviceChoice = (GridPane) loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Cafe");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(serviceChoice);
            dialogStage.setScene(scene);
            // Передаём адресата в контроллер.
            CafeOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setCafe(c);
            controller.setMain(this);
            controller.setAction();
            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showDiscount(ServiceInterface si, CafeOverviewController c)
    {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/Diskount.fxml"));
            StackPane serviceChoice = (StackPane) loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Discount Choice");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(serviceChoice);
            dialogStage.setScene(scene);
            // Передаём адресата в контроллер.
            DiskountController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setCafe(c);
            controller.setSi(si);
            controller.setDialogStage(dialogStage);
            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showBill(ListOfServices ls, double rez)
    {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/Bill.fxml"));
            AnchorPane serviceChoice = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Bill");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(serviceChoice);
            dialogStage.setScene(scene);
            // Передаём адресата в контроллер.
            BillController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setLs(ls);
            controller.setRez(rez);
            controller.setDialogStage(dialogStage);
            controller.Show(ls, rez);
            dialogStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
